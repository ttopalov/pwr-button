#!/bin/bash

PIN_NUMBER="1"
GPIO="/usr/local/bin/gpio"
SETUP_CMD="$GPIO mode $PIN_NUMBER in"
PUP_CMD="$GPIO mode $PIN_NUMBER up"
READ_CMD="$GPIO read $PIN_NUMBER"

$SETUP_CMD
$PUP_CMD

sleep 30

while true; do
  pinValue="$($READ_CMD)"
  if  [[ $pinValue -eq 0 ]]; then
    echo "User Shutdown by Power Button"
    poweroff
    exit
  fi
  sleep 0.3
done

